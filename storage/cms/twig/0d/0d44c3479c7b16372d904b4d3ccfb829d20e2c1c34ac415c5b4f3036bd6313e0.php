<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/features.htm */
class __TwigTemplate_a976ed37b97d4ff7bb76a454c9e2387601389df0023bce0758df69b11978eac8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section id=\"features\" class=\"features\">
    <div class=\"container\">
        <div class=\"section-heading text-center\">
            <h2>";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 4), "features_headline", [], "any", false, false, false, 4)]);
        echo "</h2>
            <p class=\"text-muted\">";
        // line 5
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 5), "features_subheadline", [], "any", false, false, false, 5)]);
        echo "</p>
            <hr>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12 my-auto\">
                <div class=\"container-fluid\">
                    ";
        // line 11
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 11), "features_product", [], "any", false, false, false, 11)) {
            // line 12
            echo "                        ";
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("features/items"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 13
            echo "                    ";
        } else {
            // line 14
            echo "                        ";
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("features/dummy"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 15
            echo "                    ";
        }
        // line 16
        echo "                </div>
            </div>
        </div>
    </div>
</section>
";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('scripts'        );
        // line 22
        echo "<script>
\t\$(document).ready(function(){
\t\$('.carousel').carousel()
\t});
</script>
";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/features.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  82 => 22,  80 => 21,  73 => 16,  70 => 15,  65 => 14,  62 => 13,  57 => 12,  55 => 11,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section id=\"features\" class=\"features\">
    <div class=\"container\">
        <div class=\"section-heading text-center\">
            <h2>{{ this.theme.features_headline | raw |_ }}</h2>
            <p class=\"text-muted\">{{ this.theme.features_subheadline |_ }}</p>
            <hr>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12 my-auto\">
                <div class=\"container-fluid\">
                    {% if this.theme.features_product %}
                        {% partial 'features/items' %}
                    {% else %}
                        {% partial 'features/dummy' %}
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</section>
{% put scripts %}
<script>
\t\$(document).ready(function(){
\t\$('.carousel').carousel()
\t});
</script>
{% endput %}", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/features.htm", "");
    }
}
