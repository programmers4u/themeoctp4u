<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/technology.htm */
class __TwigTemplate_d980c6d1996091d32bf90fe6fe2cb67305dffc230069c7481857c23b29b99da7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section id=\"technology\" class=\"technology\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 4), "features_list_headline", [], "any", false, false, false, 4)]);
        echo "
        </h2>
\t    
\t\t<div class=\"row\">
\t\t\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 8), "features_list", [], "any", false, false, false, 8));
        foreach ($context['_seq'] as $context["key"] => $context["feature"]) {
            // line 9
            echo "\t\t\t<div class=\"col-md-2\">
\t\t\t";
            // line 10
            if (twig_get_attribute($this->env, $this->source, $context["feature"], "icon", [], "any", false, false, false, 10)) {
                echo "\t\t    \t
\t\t\t    <img src=\"";
                // line 11
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["feature"], "icon", [], "any", false, false, false, 11));
                echo "\" alt=\"...\">
\t\t\t";
            }
            // line 12
            echo "\t\t   
\t\t    </div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['feature'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t\t</div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/technology.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 15,  65 => 12,  60 => 11,  56 => 10,  53 => 9,  49 => 8,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section id=\"technology\" class=\"technology\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t{{ this.theme.features_list_headline | raw |_ }}
        </h2>
\t    
\t\t<div class=\"row\">
\t\t\t{% for key, feature in this.theme.features_list %}
\t\t\t<div class=\"col-md-2\">
\t\t\t{% if feature.icon %}\t\t    \t
\t\t\t    <img src=\"{{ feature.icon | media }}\" alt=\"...\">
\t\t\t{% endif %}\t\t   
\t\t    </div>
\t\t\t{% endfor %}
\t\t</div>
    </div>
</section>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/technology.htm", "");
    }
}
