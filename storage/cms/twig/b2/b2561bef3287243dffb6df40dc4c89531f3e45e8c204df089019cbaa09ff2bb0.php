<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/plugins/martin/forms/components/partials/js/reset-form.js */
class __TwigTemplate_fba46a8d457763e9eae3d612c87681b3b2ede5c26dfc6cebc619ac36322f0a81 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "\$('";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "').parents('form')[0].reset();";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/plugins/martin/forms/components/partials/js/reset-form.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("\$('{{ id }}').parents('form')[0].reset();", "/home/jazz/ftp/programmers4u/plugins/martin/forms/components/partials/js/reset-form.js", "");
    }
}
