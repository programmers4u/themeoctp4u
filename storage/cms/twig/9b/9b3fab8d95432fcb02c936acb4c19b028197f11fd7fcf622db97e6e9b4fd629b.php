<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/features/items.htm */
class __TwigTemplate_9574ce9235cd934cf1d0576c486a77f61b6de1e8342f5f623cf2f9334aad80b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
\t<ol class=\"carousel-indicators\">
  \t";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 3), "features_product", [], "any", false, false, false, 3));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["feature"]) {
            // line 4
            echo "    \t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" ";
            if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 4)) {
                echo " class=\"active\"";
            }
            echo "></li>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['feature'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "  \t</ol>
  \t<div class=\"carousel-inner\">

  \t";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 9), "features_product", [], "any", false, false, false, 9));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["feature"]) {
            // line 10
            echo "    \t<div class=\"carousel-item ";
            if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 10)) {
                echo " active ";
            }
            echo "\" >
\t    \t<div class=\"row\">
";
            // line 12
            if (twig_get_attribute($this->env, $this->source, $context["feature"], "image", [], "any", false, false, false, 12)) {
                echo "\t\t    \t
\t\t    \t<div class=\"col-lg-4 my-auto\"><img src=\"";
                // line 13
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["feature"], "image", [], "any", false, false, false, 13));
                echo "\"/></div>
\t\t    \t<div class=\"col-lg-1 my-auto\"></div>
";
            }
            // line 15
            echo "\t\t    \t
\t\t    \t<div class=\"col-lg-7 my-auto\">
\t\t\t    \t<h3>";
            // line 17
            echo twig_get_attribute($this->env, $this->source, $context["feature"], "title", [], "any", false, false, false, 17);
            echo "</h3>
\t\t\t    \t";
            // line 18
            echo twig_get_attribute($this->env, $this->source, $context["feature"], "text", [], "any", false, false, false, 18);
            echo "
\t\t    \t</div>
\t    \t</div>
    \t</div>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feature'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "  \t</div>
  \t<!--
\t  <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t  \t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t    <span class=\"sr-only\">Previous</span>
\t  </a>
\t  <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t    <span class=\"sr-only\">Next</span>
\t  </a>
\t  -->
</div>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/features/items.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 23,  127 => 18,  123 => 17,  119 => 15,  113 => 13,  109 => 12,  101 => 10,  84 => 9,  79 => 6,  58 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
\t<ol class=\"carousel-indicators\">
  \t{% for key, feature in this.theme.features_product %}
    \t<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"{{key}}\" {% if loop.first %} class=\"active\"{% endif %}></li>
\t{% endfor %}
  \t</ol>
  \t<div class=\"carousel-inner\">

  \t{% for feature in this.theme.features_product %}
    \t<div class=\"carousel-item {% if loop.first %} active {% endif %}\" >
\t    \t<div class=\"row\">
{% if feature.image %}\t\t    \t
\t\t    \t<div class=\"col-lg-4 my-auto\"><img src=\"{{ feature.image | media }}\"/></div>
\t\t    \t<div class=\"col-lg-1 my-auto\"></div>
{% endif %}\t\t    \t
\t\t    \t<div class=\"col-lg-7 my-auto\">
\t\t\t    \t<h3>{{ feature.title | raw }}</h3>
\t\t\t    \t{{ feature.text | raw}}
\t\t    \t</div>
\t    \t</div>
    \t</div>
\t{% endfor %}
  \t</div>
  \t<!--
\t  <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
\t  \t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
\t    <span class=\"sr-only\">Previous</span>
\t  </a>
\t  <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
\t    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
\t    <span class=\"sr-only\">Next</span>
\t  </a>
\t  -->
</div>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/features/items.htm", "");
    }
}
