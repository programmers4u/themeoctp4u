<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/contact.htm */
class __TwigTemplate_e848f39a0ff84996a9ca1b3cf59e22ec15e45c01d6b182f5c7bb8b6c14ca67cc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section id=\"contact\" class=\"contact\">
    <div class=\"container\">
        <h2>";
        // line 3
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 3), "contact_headline", [], "any", false, false, false, 3)]);
        echo "</h2>
        ";
        // line 4
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 4), "contact_content", [], "any", false, false, false, 4)) {
            // line 5
            echo "        <p>
            ";
            // line 6
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 6), "contact_content", [], "any", false, false, false, 6)]);
            echo "
        </p>
        ";
        }
        // line 9
        echo "\t\t
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4\"><img src=\"";
        // line 11
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 11), "contact_image", [], "any", false, false, false, 11));
        echo "\"></div>
\t\t\t<div class=\"col-lg-1\"></div>
\t\t\t<div class=\"col-lg-7\">
\t\t\t\t<div class=\"form\">";
        // line 14
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("genericForm"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo "</div>
\t\t\t</div>
\t\t</div>
\t\t
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
        <ul class=\"list-inline list-social\">
            ";
        // line 21
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 21), "contact_twitter", [], "any", false, false, false, 21)) {
            // line 22
            echo "            <li class=\"list-inline-item social-twitter\">
                <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 23), "contact_twitter", [], "any", false, false, false, 23), "html", null, true);
            echo "\">
                    <i class=\"fa fa-twitter\"></i>
                </a>
            </li>
            ";
        }
        // line 28
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 28), "contact_facebook", [], "any", false, false, false, 28)) {
            // line 29
            echo "            <li class=\"list-inline-item social-facebook\">
                <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 30), "contact_facebook", [], "any", false, false, false, 30), "html", null, true);
            echo "\">
                    <i class=\"fa fa-facebook\"></i>
                </a>
            </li>
            ";
        }
        // line 35
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 35), "contact_google", [], "any", false, false, false, 35)) {
            // line 36
            echo "            <li class=\"list-inline-item social-google-plus\">
                <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 37), "contact_google", [], "any", false, false, false, 37), "html", null, true);
            echo "\">
                    <i class=\"fa fa-google-plus\"></i>
                </a>
            </li>
            ";
        }
        // line 42
        echo "        </ul>
\t\t\t</div>
\t\t</div>
    </div>
";
        // line 46
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 46), "footer_menu", [], "any", false, false, false, 46)) {
            echo "    
\t";
            // line 47
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("modules/footer.htm"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
        }
        // line 49
        echo "
</section>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 49,  129 => 47,  125 => 46,  119 => 42,  111 => 37,  108 => 36,  105 => 35,  97 => 30,  94 => 29,  91 => 28,  83 => 23,  80 => 22,  78 => 21,  66 => 14,  60 => 11,  56 => 9,  50 => 6,  47 => 5,  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section id=\"contact\" class=\"contact\">
    <div class=\"container\">
        <h2>{{ this.theme.contact_headline | raw |_ }}</h2>
        {% if this.theme.contact_content %}
        <p>
            {{ this.theme.contact_content | raw |_ }}
        </p>
        {% endif %}
\t\t
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4\"><img src=\"{{ this.theme.contact_image | media }}\"></div>
\t\t\t<div class=\"col-lg-1\"></div>
\t\t\t<div class=\"col-lg-7\">
\t\t\t\t<div class=\"form\">{% component 'genericForm' %}</div>
\t\t\t</div>
\t\t</div>
\t\t
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
        <ul class=\"list-inline list-social\">
            {% if this.theme.contact_twitter %}
            <li class=\"list-inline-item social-twitter\">
                <a href=\"{{ this.theme.contact_twitter }}\">
                    <i class=\"fa fa-twitter\"></i>
                </a>
            </li>
            {% endif %}
            {% if this.theme.contact_facebook %}
            <li class=\"list-inline-item social-facebook\">
                <a href=\"{{ this.theme.contact_facebook }}\">
                    <i class=\"fa fa-facebook\"></i>
                </a>
            </li>
            {% endif %}
            {% if this.theme.contact_google %}
            <li class=\"list-inline-item social-google-plus\">
                <a href=\"{{ this.theme.contact_google }}\">
                    <i class=\"fa fa-google-plus\"></i>
                </a>
            </li>
            {% endif %}
        </ul>
\t\t\t</div>
\t\t</div>
    </div>
{% if this.theme.footer_menu %}    
\t{% partial 'modules/footer.htm' %}
{% endif %}

</section>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/contact.htm", "");
    }
}
