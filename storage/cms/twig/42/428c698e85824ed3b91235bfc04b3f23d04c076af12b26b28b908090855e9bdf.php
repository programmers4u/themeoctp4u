<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/intro.htm */
class __TwigTemplate_c1b598e130e06a6f6abb1b140df582f0675618f8674ccdad49b5af73841af4db extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header class=\"masthead\">
\t<!-- The video -->
\t<div class=\"fullscreen-bg\">
\t\t<video loop muted autoplay poster=\"img/videoframe.jpg\" class=\"fullscreen-bg__video\">
\t\t\t<source src=\"storage/app/media/";
        // line 5
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 5), "intro_img", [], "any", false, false, false, 5);
        echo "\" type=\"video/mp4\">
    \t</video>
\t</div>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12 my-auto\">
                <div class=\"header-content\">
                    <h1 class=\"mb-5\">";
        // line 12
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 12), "intro_headline", [], "any", false, false, false, 12)]);
        echo "</h1>
                    ";
        // line 13
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 13), "intro_button_link", [], "any", false, false, false, 13)) {
            // line 14
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 14), "intro_button_link", [], "any", false, false, false, 14), "html", null, true);
            echo "\" class=\"btn btn-outline btn-xl js-scroll-trigger\">
                        ";
            // line 15
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 15), "intro_button", [], "any", false, false, false, 15)]);
            echo "
                    </a>
                    ";
        }
        // line 18
        echo "                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/intro.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 18,  64 => 15,  59 => 14,  57 => 13,  53 => 12,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header class=\"masthead\">
\t<!-- The video -->
\t<div class=\"fullscreen-bg\">
\t\t<video loop muted autoplay poster=\"img/videoframe.jpg\" class=\"fullscreen-bg__video\">
\t\t\t<source src=\"storage/app/media/{{ this.theme.intro_img | raw }}\" type=\"video/mp4\">
    \t</video>
\t</div>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-12 my-auto\">
                <div class=\"header-content\">
                    <h1 class=\"mb-5\">{{ this.theme.intro_headline | raw |_  }}</h1>
                    {% if this.theme.intro_button_link %}
                    <a href=\"{{ this.theme.intro_button_link }}\" class=\"btn btn-outline btn-xl js-scroll-trigger\">
                        {{ this.theme.intro_button | raw |_ }}
                    </a>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</header>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/intro.htm", "");
    }
}
