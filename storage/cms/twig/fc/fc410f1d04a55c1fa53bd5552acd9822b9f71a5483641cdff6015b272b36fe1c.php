<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/wow.htm */
class __TwigTemplate_5b640c4c604dd8c373c4647fdbeff96a6543a7812193df004f60d70bbc8a6cd9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"wow\" id=\"wow\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 4), "wow_headline", [], "any", false, false, false, 4)]);
        echo "
        </h2>
        
\t\t<div class=\"card-deck\">
\t\t  <div class=\"card\">
\t\t\t";
        // line 9
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 9), "wow_img1", [], "any", false, false, false, 9)) {
            echo "  
\t\t    <img src=\"";
            // line 10
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 10), "wow_img1", [], "any", false, false, false, 10));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 12
        echo "\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">";
        // line 13
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 13), "wow_headline1", [], "any", false, false, false, 13)]);
        echo "</h5>
\t\t      <p class=\"card-text\">";
        // line 14
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 14), "wow_content1", [], "any", false, false, false, 14)]);
        echo "</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t\t";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 18), "wow_img2", [], "any", false, false, false, 18)) {
            echo "  
\t\t    <img src=\"";
            // line 19
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 19), "wow_img2", [], "any", false, false, false, 19));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 21
        echo "\t\t    
\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">";
        // line 23
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 23), "wow_headline2", [], "any", false, false, false, 23)]);
        echo "</h5>
\t\t      <p class=\"card-text\">";
        // line 24
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 24), "wow_content2", [], "any", false, false, false, 24)]);
        echo "</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t";
        // line 28
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 28), "wow_img3", [], "any", false, false, false, 28)) {
            echo "  
\t\t    <img src=\"";
            // line 29
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 29), "wow_img3", [], "any", false, false, false, 29));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 31
        echo "\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">";
        // line 32
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 32), "wow_headline3", [], "any", false, false, false, 32)]);
        echo "</h5>
\t\t      <p class=\"card-text\">";
        // line 33
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 33), "wow_content3", [], "any", false, false, false, 33)]);
        echo "</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t\t";
        // line 37
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 37), "wow_img4", [], "any", false, false, false, 37)) {
            echo "  
\t\t\t  
\t\t    <img src=\"";
            // line 39
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 39), "wow_img4", [], "any", false, false, false, 39));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 41
        echo "\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">";
        // line 42
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 42), "wow_headline4", [], "any", false, false, false, 42)]);
        echo "</h5>
\t\t      <p class=\"card-text\">";
        // line 43
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 43), "wow_content4", [], "any", false, false, false, 43)]);
        echo "</p>
\t\t    </div>
\t\t  </div>
\t\t</div>    
</section>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/wow.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 43,  133 => 42,  130 => 41,  125 => 39,  120 => 37,  113 => 33,  109 => 32,  106 => 31,  101 => 29,  97 => 28,  90 => 24,  86 => 23,  82 => 21,  77 => 19,  73 => 18,  66 => 14,  62 => 13,  59 => 12,  54 => 10,  50 => 9,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"wow\" id=\"wow\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t{{ this.theme.wow_headline | raw |_ }}
        </h2>
        
\t\t<div class=\"card-deck\">
\t\t  <div class=\"card\">
\t\t\t{% if this.theme.wow_img1 %}  
\t\t    <img src=\"{{ this.theme.wow_img1 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">{{ this.theme.wow_headline1 | raw |_ }}</h5>
\t\t      <p class=\"card-text\">{{ this.theme.wow_content1 | raw |_ }}</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t\t{% if this.theme.wow_img2 %}  
\t\t    <img src=\"{{ this.theme.wow_img2 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t    
\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">{{ this.theme.wow_headline2 | raw |_ }}</h5>
\t\t      <p class=\"card-text\">{{ this.theme.wow_content2 | raw |_ }}</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t{% if this.theme.wow_img3 %}  
\t\t    <img src=\"{{ this.theme.wow_img3 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">{{ this.theme.wow_headline3 | raw |_ }}</h5>
\t\t      <p class=\"card-text\">{{ this.theme.wow_content3 | raw |_ }}</p>
\t\t    </div>
\t\t  </div>
\t\t  <div class=\"card\">
\t\t\t{% if this.theme.wow_img4 %}  
\t\t\t  
\t\t    <img src=\"{{ this.theme.wow_img4 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t    <div class=\"card-body\">
\t\t      <h5 class=\"card-title\">{{ this.theme.wow_headline4 | raw |_ }}</h5>
\t\t      <p class=\"card-text\">{{ this.theme.wow_content4 | raw |_ }}</p>
\t\t    </div>
\t\t  </div>
\t\t</div>    
</section>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/wow.htm", "");
    }
}
