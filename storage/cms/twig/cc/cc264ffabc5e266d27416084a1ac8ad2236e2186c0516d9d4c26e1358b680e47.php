<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/nav-items.htm */
class __TwigTemplate_7915d61ec276859131075121f05a1b99fddeb6f390092e96d5e9a378924f80d7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 1), "services_enabled", [], "any", false, false, false, 1)) {
            // line 2
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#services\">
        ";
            // line 4
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Services"]);
            echo "
    </a>
</li>
";
        }
        // line 8
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 8), "wow_enabled", [], "any", false, false, false, 8)) {
            // line 9
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#wow\">
        ";
            // line 11
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Way of working"]);
            echo "
    </a>
</li>
";
        }
        // line 15
        echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#features\">
        ";
        // line 17
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Our Products"]);
        echo "
    </a>
</li>
";
        // line 20
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 20), "contact_enabled", [], "any", false, false, false, 20)) {
            // line 21
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#contact\">
        ";
            // line 23
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Contact"]);
            echo "
    </a>
</li>
";
        }
        // line 27
        echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"/en\">
        ";
        // line 29
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["En"]);
        echo "
    </a>
</li>
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"/pl\">
        ";
        // line 34
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Pl"]);
        echo "
    </a>
</li>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/nav-items.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 34,  90 => 29,  86 => 27,  79 => 23,  75 => 21,  73 => 20,  67 => 17,  63 => 15,  56 => 11,  52 => 9,  50 => 8,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if this.theme.services_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#services\">
        {{ 'Services' |_ }}
    </a>
</li>
{% endif %}
{% if this.theme.wow_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#wow\">
        {{ 'Way of working' |_ }}
    </a>
</li>
{% endif %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#features\">
        {{ 'Our Products' |_ }}
    </a>
</li>
{% if this.theme.contact_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#contact\">
        {{ 'Contact' |_ }}
    </a>
</li>
{% endif %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"/en\">
        {{ 'En' |_ }}
    </a>
</li>
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"/pl\">
        {{ 'Pl' |_ }}
    </a>
</li>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/modules/nav-items.htm", "");
    }
}
