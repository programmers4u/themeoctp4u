<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/services.htm */
class __TwigTemplate_66e70b659ad1a71a8e47247bf8b705639e455a661b75b85d9ad1459116f3f10e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"services\" id=\"services\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 4), "services_headline", [], "any", false, false, false, 4)]);
        echo "
        </h2>
\t\t<div class=\"row\">
\t\t  \t<div class=\"col-lg-3\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t";
        // line 10
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 10), "services_img1", [], "any", false, false, false, 10)) {
            echo "  
\t\t\t\t\t\t<img src=\"";
            // line 11
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 11), "services_img1", [], "any", false, false, false, 11));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t\t\t\t";
        }
        // line 13
        echo "\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t      \t<h5>";
        // line 15
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 15), "services_headline1", [], "any", false, false, false, 15)]);
        echo "</h5>
\t\t\t\t\t  \t<p class=\"card-text\">";
        // line 16
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 16), "services_content1", [], "any", false, false, false, 16)]);
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t\t";
        // line 21
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 21), "services_img2", [], "any", false, false, false, 21)) {
            echo "  
\t\t    \t<img src=\"";
            // line 22
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 22), "services_img2", [], "any", false, false, false, 22));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 24
        echo "\t\t    
\t\t\t    <h5>";
        // line 25
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 25), "services_headline2", [], "any", false, false, false, 25)]);
        echo "</h5>
\t\t\t\t<p>";
        // line 26
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 26), "services_content2", [], "any", false, false, false, 26)]);
        echo "</p>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t  \t";
        // line 29
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 29), "services_img3", [], "any", false, false, false, 29)) {
            echo "  
\t\t    \t<img src=\"";
            // line 30
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 30), "services_img3", [], "any", false, false, false, 30));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 32
        echo "\t\t      \t<h5>";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 32), "services_headline3", [], "any", false, false, false, 32)]);
        echo "</h5>
\t\t\t  \t<p>";
        // line 33
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 33), "services_content3", [], "any", false, false, false, 33)]);
        echo "</p>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t\t";
        // line 36
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 36), "services_img4", [], "any", false, false, false, 36)) {
            echo "  
\t\t\t    <img src=\"";
            // line 37
            echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 37), "services_img4", [], "any", false, false, false, 37));
            echo "\" class=\"card-img-top\" alt=\"...\">
\t\t    ";
        }
        // line 39
        echo "\t\t      \t<h5>";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 39), "services_headline4", [], "any", false, false, false, 39)]);
        echo "</h5>
\t\t\t  \t<p>";
        // line 40
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 40), "services_content4", [], "any", false, false, false, 40)]);
        echo "</p>
\t\t  </div>
\t\t</div>
\t</div>    
</section>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 40,  127 => 39,  122 => 37,  118 => 36,  112 => 33,  107 => 32,  102 => 30,  98 => 29,  92 => 26,  88 => 25,  85 => 24,  80 => 22,  76 => 21,  68 => 16,  64 => 15,  60 => 13,  55 => 11,  51 => 10,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"services\" id=\"services\">
    <div class=\"container\">
    \t<h2 class=\"text-center\">
        \t{{ this.theme.services_headline | raw |_ }}
        </h2>
\t\t<div class=\"row\">
\t\t  \t<div class=\"col-lg-3\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t{% if this.theme.services_img1 %}  
\t\t\t\t\t\t<img src=\"{{ this.theme.services_img1 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t\t\t\t{% endif %}
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t      \t<h5>{{ this.theme.services_headline1 | raw |_ }}</h5>
\t\t\t\t\t  \t<p class=\"card-text\">{{ this.theme.services_content1 | raw |_ }}</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t\t{% if this.theme.services_img2 %}  
\t\t    \t<img src=\"{{ this.theme.services_img2 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t    
\t\t\t    <h5>{{ this.theme.services_headline2 | raw |_ }}</h5>
\t\t\t\t<p>{{ this.theme.services_content2 | raw |_ }}</p>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t  \t{% if this.theme.services_img3 %}  
\t\t    \t<img src=\"{{ this.theme.services_img3 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t      \t<h5>{{ this.theme.services_headline3 | raw |_ }}</h5>
\t\t\t  \t<p>{{ this.theme.services_content3 | raw |_ }}</p>
\t\t  </div>
\t\t  <div class=\"col-lg-3\">
\t\t\t{% if this.theme.services_img4 %}  
\t\t\t    <img src=\"{{ this.theme.services_img4 | media }}\" class=\"card-img-top\" alt=\"...\">
\t\t    {% endif %}
\t\t      \t<h5>{{ this.theme.services_headline4 | raw |_ }}</h5>
\t\t\t  \t<p>{{ this.theme.services_content4 | raw |_ }}</p>
\t\t  </div>
\t\t</div>
\t</div>    
</section>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/sections/services.htm", "");
    }
}
