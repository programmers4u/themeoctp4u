<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/jazz/ftp/programmers4u/themes/programmers4u/partials/genericForm/default.htm */
class __TwigTemplate_09064784ccfab71ef47b06bdef6951e9302d3ad14e09ef4ce35d39f915814644 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form data-request=\"";
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::onFormSubmit\">

    ";
        // line 3
        echo call_user_func_array($this->env->getFunction('form_token')->getCallable(), ["token"]);
        echo "

    <div id=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "_forms_flash\"></div>

    <div class=\"form-group\">
        <label for=\"email\">";
        // line 8
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Phone or E-mail address"]);
        echo "</label>
        <input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\">
\t\t<div data-validate-for=\"email\"></div>        
    </div>

    <div class=\"form-group\">
        <label for=\"comments\">";
        // line 14
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Description"]);
        echo "</label>
        <textarea id=\"comments\" name=\"comments\" rows=\"8\" cols=\"8\" class=\"form-control rounded-0\"></textarea>
    </div>

    <button id=\"simpleContactSubmitButton\" type=\"submit\" class=\"btn btn-outline\">";
        // line 18
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["send"]);
        echo "</button>

</form>";
    }

    public function getTemplateName()
    {
        return "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/genericForm/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 18,  63 => 14,  54 => 8,  48 => 5,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form data-request=\"{{ __SELF__ }}::onFormSubmit\">

    {{ form_token() }}

    <div id=\"{{ __SELF__ }}_forms_flash\"></div>

    <div class=\"form-group\">
        <label for=\"email\">{{'Phone or E-mail address'|_}}</label>
        <input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\">
\t\t<div data-validate-for=\"email\"></div>        
    </div>

    <div class=\"form-group\">
        <label for=\"comments\">{{'Description'|_}}</label>
        <textarea id=\"comments\" name=\"comments\" rows=\"8\" cols=\"8\" class=\"form-control rounded-0\"></textarea>
    </div>

    <button id=\"simpleContactSubmitButton\" type=\"submit\" class=\"btn btn-outline\">{{'send' |_}}</button>

</form>", "/home/jazz/ftp/programmers4u/themes/programmers4u/partials/genericForm/default.htm", "");
    }
}
