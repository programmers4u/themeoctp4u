<?php namespace Mak\Compileless;

use Backend;
use System\Classes\PluginBase;
use System\Classes\CombineAssets;

/**
 * compileless Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'compileless',
            'description' => 'No description provided yet...',
            'author'      => 'mak',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
	    CombineAssets::registerCallback(function($combiner) {
	        $combiner->registerBundle('~/themes/programmers4u/assets/less/programmers.less');
	    });	    
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Mak\Compileless\Components\GetContact' => 'GetContact',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'mak.compileless.some_permission' => [
                'tab' => 'compileless',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'compileless' => [
                'label'       => 'compileless',
                'url'         => Backend::url('mak/compileless/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['mak.compileless.*'],
                'order'       => 500,
            ],
        ];
    }
}
