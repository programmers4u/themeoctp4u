<?php namespace Mak\Compileless\Components;

use Cms\Classes\ComponentBase;

class GetContact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'GetContact Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
